function keys(testObject) {
    if (!testObject) {
        return []
    }
    let newArray = []
    for (let i in testObject) {
        newArray.push(i);
    }
    return newArray;
}

module.exports = keys;