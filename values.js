function values(testObject) {
    if(!testObject){
        return []
    }
    let newArray=[]
    for (let key in testObject){
        newArray.push(testObject[key]);
    }
    return newArray;
}

module.exports=values;