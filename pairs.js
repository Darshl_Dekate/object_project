function pairs(testObject) {
    if (!testObject) {
        return []
    }
    let newArray = []
    for (let i in testObject) {
        newArray.push([i, testObject[i]]);
    }
    return newArray;
}

module.exports = pairs;