function mapObject(obj, cb) {
    if(!obj){
        return [];
    }
    for(let i in obj){
        let output=cb(obj[i])
        obj[i]=output;
    }
    return obj
};

module.exports=mapObject;