function defaults(obj, object2) {
    if (!obj) {
        return [];
    }
    for (let key in object2) {
        if (!obj[key]) {
            obj[key] = object2[key];
        }
    }
    return obj;
};

module.exports = defaults;