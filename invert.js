function invert(testObject) {
    if (!testObject) {
        return []
    }
    let newArray = {}
    for (let key in testObject) {
        newArray[testObject[key]] = key;
    }
    return newArray;
}

module.exports = invert;